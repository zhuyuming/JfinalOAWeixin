package com.jfinal.weixin.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

public class HttpClientUtil {
	public String post(String url,String jsonparams) throws Exception{
		HttpClient hc=new DefaultHttpClient();
		String body=null;
		HttpPost httpPost=new HttpPost(url);

		StringEntity ue=new StringEntity(jsonparams, HTTP.UTF_8);

		httpPost.setEntity(ue);
		HttpResponse httpResponse=hc.execute(httpPost);
		HttpEntity entity=httpResponse.getEntity();
		body=EntityUtils.toString(entity);
		return body;
	}
}
